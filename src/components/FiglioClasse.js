import React, { Component } from 'react'

export class FiglioClasse extends Component {
    render() {
        return (
            <div>
                <h1>Sono la classe che invoca il Render</h1>
            </div>
        )
    }
}

export default FiglioClasse
