import logo from './logo.svg';
import './App.css';
import FiglioFunzione from './components/FiglioFunzione';
import FiglioClasse from './components/FiglioClasse';

function App() {
  return (
    <div className="container">
      <h1>Prova di titolo</h1>
      <p>Prova di testo</p>

      <FiglioFunzione />
      <FiglioClasse />
    </div>
  );
}   

export default App;
